package ru.dukentre.chaplyginnewyear;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void AboutNewYearClick(View view){
        Intent intent = new Intent(MainActivity.this, NewYearActivity.class);
        startActivity(intent);
    }

    public void NewYearImagesClick(View view){
        Intent intent = new Intent(MainActivity.this, Images.class);
        startActivity(intent);
    }
}